\documentclass[a4paper,11pt,twocolumn]{article}
\usepackage{color}        % for colored output
\usepackage[parfill]{parskip}      % Activate to begin paragraphs with an empty line rather than an indent
\usepackage[top=1in, bottom=1in, left=.7in, right=.7in]{geometry}  % document format
\usepackage{wrapfig}
\usepackage{graphicx}                % graphics, graphs, pictures
\usepackage{soul}        % underlines text nicely (command: \ul}
\usepackage{amsmath}               % for advanced mathematics
\usepackage{bm}                          % bold math symbols (command: \bm)
\usepackage{ctable}                    % Include footnotes in the table
\usepackage{longtable}     % splits long table on multiple pages
\usepackage{rotating}
\usepackage{tabularx}
\usepackage{lscape}                   % landscape
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{lscape}
\usepackage{helvet}
\usepackage{float}
\floatstyle{plaintop}
\restylefloat{table}
\usepackage[utf8]{inputenc} 
\DeclareUnicodeCharacter{FB01}{fi}


\title{\textbf{Latent Dirichlet Allocation Models for Documents Collection fitted by Collapsed Gibbs Sampling}}
\author{
        Richard Park\\
              r2park@acsmail.ucsd.edu\\
        \and
        Mofei Li\\
               mol017@ucsd.edu\\
}
\date{February 27, 2014}


\begin{document}


\maketitle

\section*{\centering{Abstract}}
When there are several different topics associated with a single document, a probabilistic topic model can help us achieve the goal of predicting more than one topic per each document i.e. learning the mixture of topics that compose a document. In this assignment, we implement Collapsed Gibbs Sampling as a training method for Latent Dirichlet Allocation. We develop our models iteratively: First, we build models on both of our data sets: classic400 and 500 New York Times articles. Second, we investigate the quality of topic assignments by looking at top ten highest-probability words for each topic and plot documents in 3D space using the principle components of $\theta$ vector to see whether the groups of documents are distinct; Then revise the model, and repeat. We conclude with a discussion of the evaluating the  goodness-of-ﬁt for the models and how to define and avoid overfitting.\\

\section{Introduction}
Topic modeling is a method for analyzing large quantities of unlabeled data. For our purpose, a topic is a probability distribution over a collection of words and a topic model is a formal statistical relationship between a group of observed and latent random variables that probabilistic procedure to generate the topics. Also, we view a document as a “bag of words”; that is, we make an exchangeability assumption for the words in the document.
\subsection{Latent Dirichlet Allocation}

We build Latent Dirichlet Allocation(LDA), a generative probabilistic topic model. In LDA, we assume that there are $K$ underlying latent topics according to which documents are generated and every word is assigned to its own topic $z_{mj} \in \{1,\ldots,K\}$. The generative process is
\\
\\
For each document:


\begin{enumerate}
  \item Draw a topic distribution, $\theta_i \sim$ Dir$\left( \alpha  \right)$, where Dir(·) is a uniform Dirichlet distribution.
  \item For each word in the document:
    \begin{enumerate}
    \item Draw a specific topic $z_{mj} \sim$ multi $(\theta_i)$ where multi(·) is a multinomial distribution
    \item Draw a word $w_{mj} \sim \beta$
    \end{enumerate}
\end{enumerate}

\begin{center}
\begin{table}[ht]
{
\begin{center}
\begin{tabular}{ ll }
  \hline
  $K$ & the total number of topics \\
  $M$ & the total number of documents\\
  $V$ & the cardinality of dictionary \\
  $n_m$ & the length of $m^{th}$ document \\
  $\theta_i$ & the probability of word $j$ \\
  $x_j$ & the count of word $j$ \\
  $w_{mj}$ & the $j^{th}$ word of $m^{th}$ document \\
  $z_{mj}$ & topic of word $w_{mj}$ \\
  $\alpha$ & Draw $\theta \sim$ Dirichlet$\left( \alpha  \right)$ \\
  $\beta$ & Draw $\phi_k \sim$ Dirichlet$\left( \beta  \right)$ \\
  \hline
\end{tabular}\end{center}
}
\caption{Notation in the report} 
\end{table}\end{center}

\subsection{Fitting using Collapsed Gibbs Sampling}
Implementations of LDA models use symmetric Dirichlet priors with ﬁxed concentration parameters $\alpha$ and $\beta$, training by collapsed Gibbs sampling means instead of inferring $\theta$ and $\phi_k$ distributions directly it infers the hidden value $z_{mj}$.Suppose that we know the value of $z$ for every word occurrence in the corpus except occurrence number $i$, Gibbs sampling draws a $z$ value for $i$ according to its distribution using the formula below:
$$p(z_i = j|\bar{z}', \bar{w})\propto \frac{q'_{jw_i}+ \beta_{w_i}}{\sum_t{q'_{jt}}+ \beta_t} \frac{n'_{mj}+ \alpha_{j}}{\sum_k{n'_{mk}+ \alpha_k}}$$
Then assume we know the $i^{th}$ value, draw a $z$ value for $i+1$, and so on. The procedure convergences in correct distribution for all $z_{mj}$ in the corpus.

\section{Algorithm and Implementation}
We implement the LDA with collapsed Gibbs sampling in the 
Python programming language. Generating a topic model for a 
corpus of documents occurs in two stages: initialization and learning. 
\subsection{Initialization}
Begin with instantiating the following matrices:
\begin{enumerate}
    \item \textbf{q\_jwt} is a matrix of size $K \times V$ where $K$ is the number of topics and
        $V$ is the number of words in the vocabulary. Every element is initialized to the 
        value of $\beta$. Every element represents the number of occurrences of word $w\_t$  
        that have been assigned topic $j$ across all documents. 
    \item \textbf{Q\_j} is an array of size $K$. Every element is initialized to the value of 
        $\beta$. Every element represents
        the total number of words assigned to topic $j$ across all documents.
    \item \textbf{n\_mj} is a matrix of size $M \times K$. Every element is initialized to $\alpha$.
        Every element represents the number of words in a single document $m$ that have 
        been assigned topic $j$.
    \item \textbf{N\_m} is an array of size $M$. Every element is initialized to $\alpha$.
        Every element represents the sum of words in a single document $m$.
    \item \textbf{z\_mi} is an array of size $M$. Every element is an array of size equal to the
        number of words in document $m$ (i.e.\ \textbf{z\_mi} is an irregular 2D matrix where every
        element is initialized to zero). Every element represents an assignment of a 
        topic $j$ to every word in the corpus.
\end{enumerate}
Once the matrices are initialized, we iterate over every word in the corpus and assign a randomly chosen topic to it. The topics are sampled from a uniform distribution. All of the data structures that were previously initialized are
updated using the sampled topic value, current document, and current word as indices. The update 
consists of increment the current value by one. The only exception is for the $z\_mi$ matrix, where the
update assigns the sampled topic to the current word.
\subsection{Learning a Topic Model}
In order to update the topic for a single word in the corpus $w_i$, the algorithm begins by performing a lookup in \textbf{z\_mi} to determine the current topic assignment. The current topic assignment 
is used to decrementing the count in the other matrices by one.

For each topic $j$ in $K$ the following calculation is performed: 
$$p_j = \frac{\textbf{q\_jwi}[j, w]}{\textbf{Q\_jt}[j]}*
\frac{\textbf{n\_mj}[doc, j]}{\textbf{N\_mk}[doc]}$$
where $w$ is the current word in the corpus and $doc$ is the document in which $w$ occurs.
The distributions for each $p_j$ are used to define a multinomial from which we sample a 
new topic assignment for $w_i$. The matrices are then updated using the new topic. We define an epoch as drawing an updated topic assignment for each word in the corpus.

\subsection{Time Complexity}
We perform the full initialization of the matrices used in our algorithm outside of our Gibbs sampling
loop. This allows the algorithm to perform lookups and updates to the matrices in 
$\mathcal{O}\left(1\right)$ for each $w_i$ in the corpus. The computation for $p_j$ is also 
$\mathcal{O}\left(1\right)$.
In order to perform a single epoch of training we must compute $p_j$ for ever $w_i$ in the corpus. 
Therefore, our implementation of LDA using collapsed Gibbs sampling is $\mathcal{O}\left(NK\right)$ 
where $N$ is the total number of words in the corpus and $K$ is the number of topics.

\section{Experiments Setup and Results}
\subsection{Classic400 dataset}
We learn topic models for two datasets. The first dataset is the Classic400 dataset, a corpus of 400 text documents
with a vocabulary of 6205 words. Each document within the dataset is selected from one of three scientific categories \cite{classic400}.
We initialize $\beta = 0.01$ and $\alpha = \frac{50}{K}$ for our experiments, these values were chosen for being within 
a reasonable range given a survey of existing literature regarding selection of hyperparameters for LDA using Collapsed 
Gibbs Sampling \cite{griffiths}\cite{heinrich}. We set the number of topics to be 3 due to our knowledge of how this specific corpus was created.

\subsection{NYTimes dataset}
NYtimes dataset is a corpus of 300000 documents which are sampled from New York Times News articles \cite{nytimes} with a 
vocabulary of 102,660 words. We initialize $\beta = 0.5$ and $\alpha = \frac{50}{K}$. The value of $\beta$ was increased for this
dataset due to our previous knowledge regarding newspaper articles. The range of topics over all possible news articles is much greater
than 3, therefore increasing the value of $\beta$ will increase the smoothness of the $\Theta$ and result in the model assigning to a 
greater number of topics with high probability. We set the number of topics $K=20$ based upon the organization of news topics on the
New York Times website. The actual number of news sections is 26, but we chose to treat several related sections as one (e.g. Movies, Television, Theater, Arts).

\subsection{Results and Analysis}
The implementation outputs the top ten words that have the highest probability for each topic. We found that the most accurate method for determining the quality of a model was to 
manually examine these words and determine how closely they were related to each other. For the Classic400 dataset, we selected the topic that occurs most often to represent a single 
document. We found empirically that 100 epochs produced the best results. 
We compared this result to the true labels and found them to be highly correlated. We also examined the top occurring words in each topic as seen in Table \ref{table:topten} 
and deduced that the topics
from which these documents were selected pertain to aviation, scientific research, and cardiology.
\begin{table}[h]
    %\resizebox{\columnwidth}{!}
    {
    \begin{center}
    \begin{tabular}{|l|l|l|}
        \hline
        \textbf{Topic 0}    & \textbf{Topic 1}    & \textbf{Topic 2}     \\ \hline
        boundary   & system     & patients    \\ \hline
        layer      & research   & ventricular \\ \hline
        wing       & scientific & cases       \\ \hline
        mach       & retrieval  & fatty       \\ \hline
        supersonic & methods    & nickel      \\ \hline
        ratio      & problems   & left        \\ \hline
        wings      & language   & acids       \\ \hline
        velocity   & noise      & aortic      \\ \hline
        shock      & science    & blood       \\ \hline
        effects    & structure  & normal      \\ \hline
    \end{tabular}\end{center}
    \caption{Top ten words for each topic of Classic400}
    \label{table:topten}
}
\end{table}

\begin{figure}%[ht!]
\centering
\includegraphics[width=60mm]{classic400_true.jpeg}
\caption{Plot of principle components of vector $\theta$ for classic400 data with true topics}
\label{Figure 1}
\end{figure}

To further examine whether the true groups of documents are distinct or not, we apply principle component analysis to vector $\theta$ get the first three principle components. 
In the Figure 1, each point represents a single document and different colors of the point indicates different true labels. 
Suppose we only get one label of topic for each document by learning model, after 100 epochs. For each document, the topic with the highest frequency associated with all words in that document is assigned to be the predicted label (See Figure 2). 

\begin{figure}[ht!]
\centering
\includegraphics[width=60mm]{classic400_pred.jpeg}
\caption{Plot of Principle Components of vector $\theta$ for classic400 data with single predicted topics}
\label{Figure 2}
\end{figure}

We performed 100 epochs on the NYtimes dataset and found that the quality of our resulting topic model was inconsistent. Some topics had a list of top ten words which were 
highly representative of a meaningful topic whereas others had a mix of words that did not seem related. In Table \ref{table:nytable} we observe that the top ten words assigned
to topic 4 are highly correlated with sports, but the relationship between words associated with topic 11 are more ambiguous. We believe that due to the increased count of words
in the NYtimes dataset a greater number of epochs is required for convergence to a distribution. 
\begin{table}[h]
    %\resizebox{\columnwidth}{!}
    {
    \begin{center}
    \begin{tabular}{|l|l|}
        \hline
        \textbf{Topic 4} & \textbf{Topic 11} \\ \hline
        teamed           & millionaire       \\ \hline
        coachable        & courted           \\ \hline
        gameday          & immigrate         \\ \hline
        seasonable       & systematic        \\ \hline
        playa            & numbered          \\ \hline
        playful          & officialdom       \\ \hline
        patriotic        & americana         \\ \hline
        guzzle           & drugged           \\ \hline
        footballer       & prisoner          \\ \hline
        gamesmanship     & zzz\_combest      \\ \hline
    \end{tabular}\end{center}
    \caption{An example of two top ten word lists from the NYTimes LDA model}
        \label{table:nytable}
}
\end{table}

\section{Discussion}
For maximum likelihood estimation of the LDA model, the log likelihood of the data is maximized with respect to the model parameters $\alpha$ and $\beta$:
$$l(\alpha,\beta) = \sum_{w \in D} log(p(w|\alpha,\beta))$$
However, The quantities $p(w|\alpha,\beta)$ for the LDA model cannot be computed in a tractable way.A variational EM procedure\cite{lda} can be used to compute the posterior distribution for estimation and the harmonic mean\cite{hanna} can be used to compute the complete likelihood $p(w,z|\alpha,\beta)$.
To measure the goodness-of-fit, mathematically we can compute the estimated held-out likelihood for LDA by variational EM or/and harmonic mean. The higher the held-out likelihood for test set the better the better the LDA model achieves goodness-of-fit.
In our experiment, we use another more practical way to measure our model, which is investigating the usefulness of the topics with the separability of the dataset by looking the highest-probability words for each topic.\\
For more mathematical details, the variational distribution likelihood is computed as below:
$$\log p(w| \alpha, \beta) = L\left ( \gamma, \phi; \alpha,\beta \right )$$
$$+ D_{KL}\left ( q(\theta, z| \gamma, \phi) || p(\theta, z|w, \alpha, \beta)\right )$$
where the second term is the Kullback-Leibler (KL) divergence between the variational distribution and the actual posterior distribution, where $\gamma$ and $\phi$ are the variational parameters.\\
The harmonic mean is shown in the following equation:
$$HM \left( {P(w|z^{(s)},\phi)}^S_{s=1} \right) = \left( \frac{1}{S} \sum_s{\frac{1}{P(w|z^{(s)},\phi)}} \right)^{-1}$$
where $S$ is the size of held-out data.\\
To avoid overfitting, we randomly pick $S$ data from the training set and compute the estimated log-likelihood by one of the two methods mentioned above. If the log-likelihood of the training data of size $S$ is much larger than that of the held-out data, it implies overfitting; 
If the likelihood of training is comparable with held-out likelihood, the learning model is not overfitting.

\begin{thebibliography}{1}
    \bibitem{classic400} Banerjee, A., Dhillon, I. S., Ghosh, J., and Sra, S.  Clustering on the unit hypersphere using von Mises-Fisher distributions.
                        Journal of Machine Learning Research (2005).
    \bibitem{lda} Reed, C. , Latent Dirichlet Allocation: Towards a Deeper
Understanding, 2012
    \bibitem{nytimes} Bache, K. and Lichman, M. (2013). UCI Machine Learning Repository (http://archive.ics.uci.edu/ml). Irvine, CA: University of California, School of Information and Computer Science.
    \bibitem{griffiths} T. Griffiths and M. Steyvers: Finding scientific topics, Proc.\ of the National Academy of Sciences (2004)
    \bibitem{heinrich} G. Heinrich: Parameter estimation for text analysis, Technical Report (2005).
    \bibitem{hanna} Wallach,H., Murray, I., Salakhutdinov, R., Mimno, D., Evaluation Methods for Topic Models (2009).
    \bibitem{elkan} Doyle, G., Elkan, C., Accounting for Burstiness in Topic Models (2009)
\end{thebibliography}
\end{document}
