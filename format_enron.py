import numpy as np
import pickle
from scipy import sparse

matrix = np.zeros((1000, 28102))

with open('dataset/docword.enron.txt') as f_in, open('enron.pickle','wb') as f_out:
    f_in.next()
    f_in.next()
    f_in.next()
    for line in f_in:
        dp = line.split()

        assert len(dp) == 3
        if int(dp[0]) == 1000:
            print dp[0]
            break
        matrix[dp[0], dp[1]] = dp[2]

    pickle.dump(sparse.csr_matrix(matrix), f_out)
