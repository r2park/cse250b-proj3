
import argparse
import logging as log
import cPickle as pickle
import numpy as np
import sys


def main():
    # Setup command line argument parsing
    parser = argparse.ArgumentParser(
        description="Parse the output pickle files from lda.py",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', help="increase verbosity of output",
                        dest='verbose', action='store_true')
    parser.add_argument('--pickle_in',
                        help='input pickle file')
    parser.add_argument('--topics',
                        help='generate a list of the top topic for each document',
                        action='store_true')
    parser.add_argument('-o',
                        help='output file name')

    log.basicConfig(level=log.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='lda.log',
                    filemode='w')
    args = parser.parse_args()

    # Setup debugging output
    if args.verbose:
        console = log.StreamHandler()
        console.setLevel(log.INFO)
        formatter = log.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        console.setFormatter(formatter)
        log.getLogger('').addHandler(console)

    with open(args.pickle_in, 'rb') as f_in, open(args.o, 'w') as f_out:
        if args.topics:
            data = pickle.load(f_in)[0]
            for doc in data[1:]:
                counts = np.bincount(np.array(doc))
                f_out.write(str(np.argmax(counts)) + '\n')

if __name__ == '__main__':
    main()
