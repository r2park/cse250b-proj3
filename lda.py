import argparse
import logging as log
import cPickle as pickle
import numpy as np
import sys

from gibbs_lda import GibbsLDA


def main():
    # Setup command line argument parsing
    parser = argparse.ArgumentParser(
        description="Latent Dirichlet Allocation",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', help="increase verbosity of output",
                        dest='verbose', action='store_true')
    parser.add_argument('--learn',
                        help='learn a topic model using Gibbs sampling',
                        action='store_true')
    parser.add_argument('--test',
                        help='test data using a model')
    parser.add_argument('--file_in',
                        help='input Matlab file in sparse matrix format')
    parser.add_argument('--pickle_in',
                        help='input pickle file')
    parser.add_argument('--process',
                        help='process files',
                        action='store_true')
    parser.add_argument('--pickle_out',
                        help='output pickle file name',
                        dest='pickle_out')
    parser.add_argument('--epochs',
                        help='number of epochs to run',
                        type=int)
    parser.add_argument('-k',
                        help='number of topics to use',
                        type=int,
                        default=3)

    log.basicConfig(level=log.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='lda.log',
                    filemode='w')
    args = parser.parse_args()

    # Setup debugging output
    if args.verbose:
        console = log.StreamHandler()
        console.setLevel(log.INFO)
        formatter = log.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        console.setFormatter(formatter)
        log.getLogger('').addHandler(console)

    # Load the data
    data = []
    isDataSparse = False
    if args.pickle_in:
        print '[*] Loading pickle: {0}'.format(args.pickle_in)
        data = pickle.load(open(args.pickle_in, 'rb'))
        vocab = []
        with open('dataset/vocab.nytimes.txt') as f:
            for line in f:
                vocab.append(line)

        isDataSparse = True
    elif args.process:
        pass
    elif args.file_in:
        import scipy.io
        isDataSparse = True
        rawdata = scipy.io.loadmat(args.file_in)
        data = rawdata['classic400']
        vocab = rawdata['classicwordlist']
        vocab = [x[0][0] for x in vocab]
    else:
        print "[!] Must give an input file"
        sys.exit(1)

    # Test an existing model
    if args.test:
        print '[*] TESTING'

    # Train a new model
    elif args.learn:
        print '[*] LEARNING'
        lda_gibbs = GibbsLDA(data, isDataSparse, args.verbose
                             , args.epochs, args.k, vocab)
        topic_matrix = lda_gibbs.learn()

        #print topic_matrix
        if args.pickle_out:
            with open(args.pickle_out, 'wb') as f:
                pickle.dump(topic_matrix, f)

def file_len(fname):
    """ Count total number of lines in a file """
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


if __name__ == '__main__':
    main()
