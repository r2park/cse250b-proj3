import numpy as np



class GibbsLDA:

    def __init__(self, data, isDataSparse, verbose, epoch_max, K, vocab):
        self.vocab = vocab
        self.epoch_max = epoch_max
        self.v = verbose
        self.data = data

        # The matlab file for project3 is in a sparse format so we handle it
        # separately.
        if isDataSparse:
            self.doc_idx, self.vocab_idx = np.nonzero(self.data)
        else:
            pass

        self.doc_total = np.amax(self.doc_idx) + 1
        self.vocab_size = np.amax(self.vocab_idx) + 1
        self.K = K
        self.alpha = 0.5
        self.beta = 0.5

        # The number of times that a word, w_i, occurs with topic j in the
        # entire corpus. q_jw_i is an K x V matrix
        self.q_jw_i = np.zeros((K, self.vocab_size)) + self.beta

        # The sum of words assigned to a topic.
        self.Q_jt = np.zeros(K) + self.beta

        # The number of words that are assigned topic j in document m_j.
        # n_mj is an M x K matrix
        self.n_mj = np.zeros((self.doc_total, K)) + self.alpha

        # The sum of words in a given document m
        self.N_mk = np.zeros(self.doc_total)

        # z_mi is an array of arrays of all words in the corpus.
        self.z_mi = [[] for x in xrange(self.doc_total)]

        # Initialize with random topics
        for doc, word in zip(self.doc_idx, self.vocab_idx):
            for i in xrange(int(self.data[doc, word])):
                z = np.random.randint(K-1)
                self.z_mi[doc].append(z)
                self.q_jw_i[z, word] += 1
                self.Q_jt[z] += 1
                self.n_mj[doc, z] += 1
                self.N_mk[doc] += 1

    def learn(self):
        doc_prev = -99
        for epoch in xrange(self.epoch_max):
            print 'Epoch: {0}'.format(epoch)
            for doc, word in zip(self.doc_idx, self.vocab_idx):
                if doc != doc_prev:
                    word_offset = 0
                    doc_prev = doc
                for i in xrange(int(self.data[doc, word])):
                    # Remove the current word and topic from q and n
                    z_old = self.z_mi[doc][word_offset]
                    self.q_jw_i[z_old, word] -= 1
                    self.Q_jt[z_old] -= 1
                    self.n_mj[doc, z_old] -= 1
                    self.N_mk[doc] -= 1

                    p_j = np.zeros(self.K)

                    for j in xrange(self.K):
                        p_j[j] = (self.q_jw_i[j, word] / self.Q_jt[j] *
                                  self.n_mj[doc, j] / (self.N_mk[doc] + self.alpha))
                    if self.v:
                        print 'P_j: {0}'.format(p_j)

                    z_new = np.random.multinomial(1, p_j / p_j.sum()).argmax()

                    self.z_mi[doc][word_offset] = z_new
                    self.q_jw_i[z_new, word] += 1
                    self.Q_jt[z_new] += 1
                    self.n_mj[doc, z_new] += 1
                    self.N_mk[doc] += 1

                    word_offset += 1

        perplexity = self._perplexity()
        print '[*] Perplexity: {0}'.format(perplexity)
        print '[*] Top 10 words for each topic in descending order of probability'
        for j in xrange(self.K):
            top_indices = self.q_jw_i[j].argsort()[-10:][::-1]
            print 'Topic {0}:'.format(j)
            for t in top_indices:
                print '    {0}'.format(self.vocab[t])


        return (self.z_mi, perplexity)

    def _perplexity(self):
        phi = self.q_jw_i / self.Q_jt[:, np.newaxis]
        log_per = 0
        N = 0
        Kalpha = self.K * self.alpha
        doc_prev = -99

        for doc, word in zip(self.doc_idx, self.vocab_idx):
                if doc != doc_prev:
                    theta = self.n_mj[doc] / (self.N_mk[doc] + Kalpha)
                    word_offset = 0
                    doc_prev = doc
                for i in xrange(int(self.data[doc, word])):
                    log_per -= np.log(np.inner(phi[:, word_offset], theta))
                    N += 1

                    word_offset += 1
        return np.exp(log_per / N)
